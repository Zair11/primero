***Settings***
Resource    recursos.robot


*** Test Cases***
G001 Busqueda de palabras en google
    Abrir Navegador y Esperar Logo
    Input Text    xpath=/html/body/div[1]/div[3]/form/div[1]/div[1]/div[1]/div/div[2]/input     ${Buscarpalabra}
    Click Element       xpath=/html/body/div[1]/div[3]/form/div[1]/div[1]/div[3]/center/input[1]
    Title Should Be     ${Buscarpalabra} - Buscar con Google
    Page Should Contain     ${Buscarpalabra}
    Close Browser

G002 No escribir nada
    Abrir Navegador y Esperar Logo
    Click Element       xpath=/html/body/div[1]/div[3]/form/div[1]/div[1]/div[3]/center/input[1]
    Title Should Be     Google
    Close Browser

G003 Hola
    Abrir Navegador y Esperar Logo

