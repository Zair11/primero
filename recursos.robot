***Settings***
Documentation           Existe en un documento de textos los pasos manuales
...                     Esta es mi primera automatización
Library                 Selenium2Library

***Variables***
${Buscarpalabra}    casos de prueba
${Navegador}    chrome
${Url}      https://www.google.es/

***Keywords***
Abrir Navegador y Esperar Logo
    Open Browser    ${Url}      ${Navegador}
    Wait Until Element Is Visible   xpath=/html/body/div[1]/div[2]/div/img